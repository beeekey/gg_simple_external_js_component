# gg_simple_external_js_component


This is a very simple, plain JS web component, which one can add to an already build geogirafe project.

It adds a button to the menubar. When clicked on it, the map moves to Laax and zooms in.

## integration

### adding the code to the project
copy the `test_component` folder to the root folder of your project


### integration in index.html
add the following to the `<head></head>` of your `index.html`:

```html
<script type="module" src="./test_component/initialize.js"></script>
```


add the element to the geogirafe header f.e. `<header></header>` with the following code:

```html
<girafe-testcomponent></girafe-testcomponent>
```
